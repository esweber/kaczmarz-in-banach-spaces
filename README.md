# README #

This repository houses supplemental material associated to the article "The Kaczmarz Algorithm in Banach Spaces", by Anna Aboud and Eric S. Weber.

### What does this repository contain? ###

* Appendix of the article as a standalone .pdf document
* SAGE code for some of the matrix computations

### How do I get set up? ###

* Download repository
* SAGE code requires access to a SAGE server or local installation (sagemath.org)

### Who do I talk to? ###

* Repo owner: Eric Weber, Department of Mathematics, Iowa State University (esweber@iastate.edu)
