sage: F=matrix(3,3,[[1, 0, 0],[1, 1, 0],[.5, .5, 1]])
sage: D=matrix(3,3,[[1, 1, 2],[1, 1, 1],[.5, .5, 1]])
sage: B=matrix(3,3,[[0, 0, -2],[0, -.5, 1],[0, .25, -.5]])
sage: C=matrix(3,3,[[0, 0, 0],[0, -.25, .5],[0, .125, -.25]])
sage: R=matrix(3,3,[[1, 0, 0],[1, 1, 0],[2,1, 1]])
sage: S=matrix(3,3,[[1, 1, .5],[1, 1, .5],[2, 1, 1]])
sage: T=matrix(3,3,[[.5, -.5, 0],[0,0, 0],[-1,1,0]])
sage: I=matrix(3,3,[[1, 0, 0],[0, 1, 0],[0, 0, 1]])
sage: W=matrix(3,3,[[.5, -.5, -.5],[0, 0, 0],[-1, 1, 0]])
sage: D*F^(-1)+F*B
sage: D*F^(-1)+D*B+F*C
sage: D*C-.5*F*C
sage: S*R^(-1)+R*W
sage: S*R^(-1)+S*W+R*T
sage: S*T
sage: (transpose(R)-I)*(transpose(R)+(F^(-1)-I)*(transpose(R)-I))+transpose(S)*B*(transpose(R)-I)+2*transpose(S)*C*(transpose(R)-I)
sage: (transpose(R)-I)*(transpose(R)+B*transpose(S)+(F^(-1)-I)*(transpose(R)-I))+transpose(S)*B*(transpose(R)-I)+2*transpose(S)*C*transpose(S)+2*transpose(S)*C*(transpose(R)-I)
sage: (transpose(R)-I)*C*transpose(S)+transpose(S)*C*transpose(S)
sage: (transpose(R)-I)*B*(transpose(R)-I)+2*transpose(S)*C*(transpose(R)-I)
sage: (transpose(R)-I)*(C*transpose(S)+B*(transpose(R)-I))+transpose(S)*C*transpose(S)+2*transpose(S)*C*(transpose(R)-I)
sage: (transpose(R)-I)*(transpose(S)+(F^(-1)-I)*transpose(S))+transpose(S)*(transpose(R)+B*transpose(S)+(F^(-1)-I)*(transpose(R)-I))+transpose(S)*B*(transpose(R)-I)+2*transpose(S)*C*transpose(S)+2*transpose(S)*C*(transpose(R)-I)
sage: (transpose(R)-I)*(transpose(S)+B*transpose(S)+(F^(-1)-I)*transpose(S))+transpose(S)*(transpose(R)+C*transpose(S)+B*transpose(S)+(F^(-1)-I)*(transpose(R)-I))+transpose(S)*B*(transpose(R)-I)+transpose(S)*C*transpose(S)+2*transpose(S)*C*transpose(S)+2*transpose(S)*C*(transpose(R)-I)
sage: transpose(D)*W*(transpose(F)-I)
sage: transpose(D)*T*(transpose(F)-I)
sage: (transpose(F)-I)*(transpose(F)+(R^(-1)-I)*(transpose(F)-I))
sage: ((transpose(F))^(-1)-I)*(R^(-1)-I)+transpose(B)*W+2*transpose(C)*T
sage: ((transpose(F))^(-1)-I)*W+transpose(B)*T+2*transpose(C)*T
sage: ((transpose(F))^(-1)-I)*T+transpose(B)*T+2*transpose(C)*T
sage: transpose(C)*W
sage: transpose(C)*T
sage: transpose(C)*(R^(-1)-I)
sage: -1*transpose(C)*T
sage: transpose(B)*(R^(-1)-I)
sage: transpose(S)*(F^(-1)-I)*transpose(S)+transpose(S)*transpose(S)+transpose(S)*B*transpose(S)+2*transpose(S)*C*transpose(S)
sage: (transpose(R)-I)*B*transpose(S)+2*transpose(S)*C*transpose(S)
